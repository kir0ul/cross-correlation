---
jupyter:
  jupytext:
    formats: ipynb,md
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.0'
      jupytext_version: 1.0.4
  kernelspec:
    display_name: .venv
    language: python
    name: .venv
---

```python
import sys
sys.path
sys.executable
```

```python
import os
from scipy.io import loadmat
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
```

```python
DATA = "Wilson_preprocessed_Dataset.mat"
DATA_PATH = os.path.join("Data", DATA)
mat = loadmat(DATA_PATH)
```

```python
# Basic parameters - not needed here
# Odors 2 and 3 are mineral oil and air-flow
# Imaging frequency 20 Hz
# OdorNum = 7; TrialNum = 5; TrialDuration = 600; # unit: frame

# Input matrix dimension: time (unit: frame), cells, trials, odors
Input_Matrix = mat["LEC_Pooled_4D_Dataset"] 
LEC_Pooled_2D_Dataset = mat["LEC_Pooled_2D_Dataset"]
```

```python
LEC_Pooled_2D_Dataset.shape
```

```python
Input_Matrix.shape
```

```python
# Correlation time interval
Start = 200
End = 260

# Mean over the stimulation period and reducing 4D to 3D: cell, trial, odor
Mean_Dffs_CrssCorr = np.mean(Input_Matrix[Start:End,:,:,:], axis=0)
Mean_Dffs_CrssCorr.shape

# Concatenating the all trials of odors and reducing 3D to 2D:
# Dimensions: cell, combined trial-odor
TrialOdor_Matrix = Mean_Dffs_CrssCorr[:,:]
TrialOdor_Matrix.shape
```

```python
LEC_Pooled_2D_Dataset_df = pd.DataFrame(LEC_Pooled_2D_Dataset)
corr = LEC_Pooled_2D_Dataset_df.corr()

# Spearman cross-correlation coefficients
CrossCorr_Coeff = TrialOdor_Matrix..corr(method="spearman")
```

```python
# Matplotlib

f = plt.figure(figsize=(19, 15))
plt.matshow(corr, fignum=f.number)
# plt.xticks(range(LEC_Pooled_2D_Dataset_df.shape[1]), LEC_Pooled_2D_Dataset_df.columns, fontsize=14, rotation=45)
# plt.yticks(range(LEC_Pooled_2D_Dataset_df.shape[1]), LEC_Pooled_2D_Dataset_df.columns, fontsize=14)
cb = plt.colorbar()
cb.ax.tick_params(labelsize=14)
plt.title('Correlation Matrix', fontsize=16);
```

```python
# Seaborn

sns.set(style="white")

# Generate a mask for the upper triangle
mask = np.zeros_like(corr, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))

# Generate a custom diverging colormap
cmap = sns.diverging_palette(220, 10, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})
```

```python

```
